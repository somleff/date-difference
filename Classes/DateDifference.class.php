<?php

class DateDifference
{
    public $years;
    public $months;
    public $days;
    public $total_days;
    public $invert;


    public function __construct( $date_start, $date_end )
    {
        $date_start = explode( '-', $date_start );
        $date_end = explode( '-', $date_end);

        $this->years = $date_end[0] - $date_start[0];
        $this->months = $date_end[1] - $date_start[1];
        $this->days = $date_end[2] - $date_start[2];
        $this->total_days = $this->years * 365 + $this->months * 12 + $this->days;

        if($date_start > $date_end)
        {
            $this->invert = 1;
        }
        else
        {
            $this->invert = 0;
        }
    }


    public function getDifference()
    {
        echo "Difference Between Dates:" ."<hr>";
        echo "Years: " .$this->years ."<br>";
        echo "Month: " .$this->months ."<br>";
        echo "Days: " .$this->days ."<br>";
        echo "Total Days:" .$this->total_days ."<br>";
        echo "InvertTrue: " .$this->invert;
    }
}