<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once 'Classes/DateDifference.class.php';

$dates = new DateDifference( '1984-10-06', '2015-09-21');
$dates->getDifference();