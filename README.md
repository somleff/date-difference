Determining the Difference Between Dates

Condition.
Calculate the difference between two arbitrary dates.
The difference is measured in years ($ years), months ($ months), days ($ days).
Let's calculate the total number of days between dates ($ total_days).
$ invert prints true or false if the start date is greater than the end date.

Performance.
Without the use of built-in functions of PHP.

-------------------------------------------------

Определение разницы между датами

Условие.
Посчитаем разницу между двумя произвольными датами.
Разницу измеряем в годах ($years), месяцах ($months), днях ($days).
Подсчитаем общее количество дней между датами($total_days).
$invert выводит true или false при условии, что дата старта больше даты конца.

Выполнение.
Без применения встроенных функций PHP.